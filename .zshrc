source $HOME/.zshconfig/antigen/antigen.zsh
source $HOME/.zshconfig/powerline_config
source $HOME/.zshconfig/antigen_config

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vim'
else
    export EDITOR='mvim'
fi
export SSH_KEY_PATH="~/.ssh/rsa_id"

export DEVROOT=$HOME/Dev
export EDITOR=vim

# Go
export GOROOT=$HOME/go
export GOPATH=$HOME/Dev/go
export GOBIN=$GOROOT/bin
export PATH=$GOBIN:$PATH

# Home Bin
export PATH=$HOME/bin:$PATH

# Python Bin
export PATH=$HOME/.local/bin:$PATH

# Exercism Completion
if [ -f ~/.config/exercism/exercism_completion.zsh ]; then
    source ~/.config/exercism/exercism_completion.zsh
fi

source $HOME/.zshconfig/aliases

# Credentials
source $HOME/.zshconfig/credentials/aws_credentials

